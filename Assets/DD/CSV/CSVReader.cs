﻿using UnityEngine;
using System.Collections;
using System.IO;

/// <summary>
/// Get a file made with the CSV Writer and get the lines.
/// See CSVTest for an example.
/// 
/// *******************************************************************************
/// EDITORS NOTE: if a string contains a ',' the comma is replaced with "&c;"  
/// This is fine for plain text files but be wary if you are using encryption code
/// as it might create a &c; somewhere where you didn't place a comma.
/// *******************************************************************************
/// </summary>
public class CSVReader {


    /// <summary>
    /// Get the file and return a array of lines with a text file (then pass the individual lines into GetArgumentsInLine())
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns>a series of lines</returns>
    public static string[] GetFile(string fileName)
    {
        if(!File.Exists(fileName))
        {
            Debug.Log("Filename " + fileName + " does not exist.");
            return null;
        }
        StreamReader file = new StreamReader(fileName);
        string[] lines = file.ReadToEnd().Split('\n');
        file.Close();
        return lines;
    }

    /// <summary>
    /// Get the file and return an array of lines with a text asset (then pass the individual lines into GetArgumentsInLine())
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public static string[] GetFile(TextAsset fileName)
    {
        string[] lines = fileName.text.Split(',');
        return lines;
    }

    /// <summary>
    /// Returns an object list.  You should use object[0] as a (string) for the object name.
    /// </summary>
    /// <param name="line"></param>
    /// <returns>An array of objects</returns>
    public static object[] GetArgumentsInLine(string line)
    {
        
        string[] args = line.Split(',');
        object[] returnArgs = new object[args.Length];
        for (int i = 0; i < args.Length; i++)
        {
            int myInt;
            float myFloat;
            bool myBool;

            if (int.TryParse(args[i], out myInt))
                returnArgs[i] = myInt;
            else if (float.TryParse(args[i], out myFloat))
                returnArgs[i] = myFloat;
            else if (bool.TryParse(args[i], out myBool))
                returnArgs[i] = myBool;
            else
            {
                args[i] = args[i].Replace("&c;", ",");
                returnArgs[i] = args[i];
            }
        }
        return returnArgs;
    }
}
