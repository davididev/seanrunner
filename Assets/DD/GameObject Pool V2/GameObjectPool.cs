﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Creates a pool of objects that are turned off/on instead of being created/destroyed.  
/// This is a recommended approach for mobile games 
/// Treat OnEnable() as Start() and OnDisable() as OnDesroy().
/// </summary>
public class GameObjectPool : MonoBehaviour
{



    private static Dictionary<string, GameObject[]> gameObjectPool = new Dictionary<string, GameObject[]>();
    public void ResetScene(Scene prev, Scene next)
    {
        gameObjectPool.Clear();
    }
    /// <summary>
    /// Add an item to the Game Object pool.
    /// </summary>
    /// <param name="name">The name to identify as.  This is a unique key and cannot be reproduced</param>
    /// <param name="prefab">The prefab to instantiate.  It will automatically be set to inactive.</param>
    /// <param name="size">How many to instantiate.  This cannot be changed.</param>
    /// <returns>True if the entry is inited, false if it already exists.</returns>
    public static bool InitPoolItem(string name, GameObject prefab, int size)
    {
        if (gameObjectPool.ContainsKey(name))
        {
            Debug.Log("The key " + name + " already exists.");
            return false;
        }

        GameObject[] pool = new GameObject[size];
        for(int i = 0; i < size; i++)
        {
            pool[i] = GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
            pool[i].SetActive(false);
        }

        gameObjectPool.Add(name, pool);

        return true;
    }

    /// <summary>
    /// Get an instance of an object you put in the pool
    /// </summary>
    /// <param name="key">The name of the pool instance (see InitPoolItem)</param>
    /// <param name="pos">Position where it spawns</param>
    /// <param name="rotation">Euler angles to be facing</param>
    /// <returns>The instance, or null if it fails</returns>
    public static GameObject GetInstance(string key, Vector3 pos, Vector3 rotation)
    {
        GameObject inst = null;
        GameObject[] mypool;
        if(gameObjectPool.TryGetValue(key, out mypool))
        {
            for(int i = 0; i < mypool.Length; i++)
            {
                if(mypool[i].activeSelf == false)
                {
                    mypool[i].transform.position = pos;
                    mypool[i].transform.eulerAngles = rotation;
                    mypool[i].SetActive(true);
                    return mypool[i];
                    
                }
            }
        }
        else
        {
            Debug.LogError("Key " + key + " does not exist.");
            return null;
        }

        Debug.LogError("Not enough objects in the pool of " + key);
        return null;
    }

    /// <summary>
    /// Get an instance of an object you put in the pool
    /// </summary>
    /// <param name="key">The name of the pool instance (see InitPoolItem)</param>
    /// <param name="pos">Position where it spawns</param>
    /// <param name="rotation">Quaternion to be facing</param>
    /// <returns>The instance, or null if it fails</returns>
    public static GameObject GetInstance(string key, Vector3 pos, Quaternion rotation)
    {
        Vector3 rot = rotation.eulerAngles;
        return GetInstance(key, pos, rot);
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        SceneManager.activeSceneChanged += this.ResetScene;
    }
	
	void OnDisable()
	{
		gameObjectPool.Clear();
		SceneManager.activeSceneChanged -= ResetScene;
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
