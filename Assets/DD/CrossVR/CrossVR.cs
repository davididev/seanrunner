﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class CrossVR : MonoBehaviour
{

    public static Vector3 headPosition, realRotation;
    public GameObject noloRoot, steamRoot, leftHand, rightHand;
	public static Transform LeftHandTransform, RightHandTransform;
	public GameObject headRotator, mainCamera;
    public Transform noloRightHand, noloLeftHand, noloHMD, noloHMDRoot, steamHMDRoot, steamHMD, steamRightHand, steamLeftHand;

	public static Transform CurrentHMD, CurrentHMDRoot;
    public struct HandInfo
    {
        public bool triggerPressed;
        public bool triggerDown;
        public bool triggerUp;
        public bool gripPressed;
        public bool gripDown;
        public bool gripUp;
        public bool appPressed;
        public bool appUp;
        public bool appDown;
        public bool thumbPressed;
        public bool thumbUp;
        public bool thumbDown;
        public Vector2 joystick;
        public Vector3 linearAcceleration;
        public Vector3 angularVelocity;
		public Vector3 realPosition;
		public Vector3 realEulerAngles;
		public Quaternion realQuaternion;
    }

    public static HandInfo[] hands = new HandInfo[2];
	public static int Right = 0, Left = 1;
	/*
    public static HandInfo Right
    {
        get { return hands[0]; }
    }
    public static HandInfo Left
    {
        get { return hands[1]; }
    }
	*/


    /// <summary>
    /// Create a haptic pulse
    /// </summary>
    /// <param name="freq"></param>
    /// <param name="right"></param>
    public static void HapticPulse(int freq, bool right)
    {
        
		if(useNolo)
		{
			NoloVR_Controller.NoloDevice info = NoloVR_Controller.GetDevice(NoloDeviceType.LeftController);
				if(right)
					info = NoloVR_Controller.GetDevice(NoloDeviceType.RightController);
			info.TriggerHapticPulse(freq / 100);
		}
		else
		{
			
			int cs = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Leftmost);
			if(right)
				cs = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Rightmost);
			
			SteamVR_Controller.Input(cs).TriggerHapticPulse((ushort) freq);
			
		}
    }

	private static bool useNolo = false;
	private bool firstRun = true;
    // Start is called before the first frame update
    void Start()
    {
		
	if(Application.isEditor)
		useNolo = true;
	if (Application.platform == RuntimePlatform.Android)
		useNolo = true;

        //Use Nolo CV1
		if(useNolo)
		{
			noloRoot.SetActive(true);
			steamRoot.SetActive(false);
			
			leftHand.transform.parent = noloLeftHand.transform;
				rightHand.transform.parent = noloRightHand.transform;
				if(Application.isEditor == false)
				{
					//Not editor.  Compensate for the two meters by pushing it one meter ahead.
					leftHand.transform.GetChild(0).localPosition = new Vector3(0f, 0f, 2f);
					rightHand.transform.GetChild(0).localPosition = new Vector3(0f, 0f, 2f);
				}
				else
				{
					leftHand.transform.GetChild(0).localPosition = new Vector3(0f, 0f, 1f);
					rightHand.transform.GetChild(0).localPosition = new Vector3(0f, 0f, 1f);
				}
			
		
			//noloRoot.transform.parent = mainCamera.transform;
		
			//noloHMD.transform.parent = mainCamera.transform;
		}
		else
		{
			
			//Use Steam VR
			noloRoot.SetActive(false);
			steamRoot.SetActive(true);
			leftHand.transform.parent = steamLeftHand.transform;
			rightHand.transform.parent = steamRightHand.transform;
				
				leftHand.transform.GetChild(0).localPosition = new Vector3(0f, 0f, 0f);
				rightHand.transform.GetChild(0).localPosition = new Vector3(0f, 0f, 0f);
			//headRotator.transform.parent = steamHMD.transform;
			//steamRoot.transform.parent= headRotator.transform;
			
		}
		
		RightHandTransform = rightHand.transform.GetChild(0).transform;
		LeftHandTransform = leftHand.transform.GetChild(0).transform;
    }


    // Update is called once per frame
    void Update()
    {
        //Use Nolo CV1
		
		if(useNolo)
		{
		CurrentHMD = noloHMD;
		CurrentHMDRoot = noloHMDRoot;
		
			if(NoloVR_Controller.devices == null)
				return;
			if(firstRun)
			{
				
				firstRun = false;
			}
			noloHMDRoot.transform.position = headRotator.transform.position;
			noloHMDRoot.transform.eulerAngles = headRotator.transform.eulerAngles;
			#if NOLO_6DOF
			for (int i = 0; i < 2; i++)
			{
				//NoloVR_Controller.NoloDevice info = NoloVR_Controller.devices[i];
				NoloVR_Controller.NoloDevice info = NoloVR_Controller.GetDevice(NoloDeviceType.LeftController);
				if(i == Right)
					info = NoloVR_Controller.GetDevice(NoloDeviceType.RightController);
			
				hands[i].appPressed = info.GetNoloButtonPressed(NoloButtonID.Menu);
				hands[i].gripPressed = info.GetNoloButtonPressed(NoloButtonID.Grip);
				hands[i].triggerPressed = info.GetNoloButtonPressed(NoloButtonID.Trigger);
				hands[i].thumbPressed = info.GetNoloButtonPressed(NoloButtonID.TouchPad);
				hands[i].appDown = info.GetNoloButtonDown(NoloButtonID.Menu);
				hands[i].gripDown = info.GetNoloButtonDown(NoloButtonID.Grip);
				hands[i].triggerDown = info.GetNoloButtonDown(NoloButtonID.Trigger);
				hands[i].thumbDown = info.GetNoloButtonDown(NoloButtonID.TouchPad);
				hands[i].appUp = info.GetNoloButtonUp(NoloButtonID.Menu);
				hands[i].gripUp = info.GetNoloButtonUp(NoloButtonID.Grip);
				hands[i].triggerUp = info.GetNoloButtonUp(NoloButtonID.Trigger);
				hands[i].thumbUp = info.GetNoloButtonDown(NoloButtonID.TouchPad);
	
			
				Vector2 j = info.GetAxis();
				j.x = Mathf.Round(j.x * 2f) / 2f;
				j.y = Mathf.Round(j.y * 2f) / 2f;
			
				hands[i].joystick = j;
				//hands[i].angularVelocity = info.GetPose().vecAngularVelocity;
				//hands[i].linearAcceleration = info.GetPose().vecVelocity;

			}
			#endif
		}
		else
		{


		InputTracking.disablePositionalTracking = true;
        //Use Steam VR
		CurrentHMD = steamHMD;
		CurrentHMDRoot = steamHMDRoot;
			steamHMDRoot.transform.position = headRotator.transform.position;
			steamHMDRoot.transform.eulerAngles = headRotator.transform.eulerAngles;
			for (int i = 0; i < 2; i++)
			{
            //SteamVR_Controller.Device cs = SteamVR_Controller.Input(i);
			int cs = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Leftmost);
			if(cs == -1)
				return;
			if(i == Right)
				cs = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Rightmost);
			/*
			hands[i].angularVelocity = SteamVR_Controller.Input(cs).angularVelocity;
			hands[i].linearAcceleration = SteamVR_Controller.Input(cs).velocity;
			hands[i].appPressed = SteamVR_Controller.Input(cs).GetPress(Valve.VR.EVRButtonId.k_EButton_ApplicationMenu);
				hands[i].appDown = SteamVR_Controller.Input(cs).GetPressDown(Valve.VR.EVRButtonId.k_EButton_ApplicationMenu);
				hands[i].appUp = SteamVR_Controller.Input(cs).GetPressUp(Valve.VR.EVRButtonId.k_EButton_ApplicationMenu);

				hands[i].gripPressed = SteamVR_Controller.Input(cs).GetPress(Valve.VR.EVRButtonId.k_EButton_Grip);
				hands[i].gripDown = SteamVR_Controller.Input(cs).GetPressDown(Valve.VR.EVRButtonId.k_EButton_Grip);
				hands[i].gripUp = SteamVR_Controller.Input(cs).GetPressUp(Valve.VR.EVRButtonId.k_EButton_Grip);

				hands[i].triggerPressed = SteamVR_Controller.Input(cs).GetPress(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
				hands[i].triggerDown = SteamVR_Controller.Input(cs).GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
				hands[i].triggerUp = SteamVR_Controller.Input(cs).GetPressUp(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);

				hands[i].thumbPressed = SteamVR_Controller.Input(cs).GetPress(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
				hands[i].thumbDown = SteamVR_Controller.Input(cs).GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
				hands[i].thumbUp = SteamVR_Controller.Input(cs).GetPressUp(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
			*/
				//hands[i].joystick = SteamVR_Controller.Input(cs).GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
				
				if(i == Left)
				{
					Vector2 ohJoy = new Vector2(Input.GetAxis("Left Horizontal"), Input.GetAxis("Left Vertical"));
					hands[i].joystick = ohJoy.normalized;
					bool last = hands[i].thumbPressed;
					hands[i].thumbPressed = !Mathf.Approximately(Input.GetAxis("Left Pad Pressed"), 0f);
					hands[i].thumbUp = (hands[i].thumbPressed == false && last == true);
					hands[i].thumbDown = (hands[i].thumbPressed == true && last == false);
						
					
					
					 last = hands[i].triggerPressed;
					hands[i].triggerPressed = !Mathf.Approximately(Input.GetAxis("Left Trigger"), 0f);
					hands[i].triggerUp = (hands[i].triggerPressed == false && last == true);
					hands[i].triggerDown = (hands[i].triggerPressed == true && last == false);
					
					 last = hands[i].gripPressed;
					hands[i].gripPressed = !Mathf.Approximately(Input.GetAxis("Left Grip"), 0f);
					hands[i].gripUp = (hands[i].gripPressed == false && last == true);
					hands[i].gripDown = (hands[i].gripPressed == true && last == false);
					
					last = hands[i].appPressed;
					hands[i].appPressed = !Mathf.Approximately(Input.GetAxis("Left Menu"), 0f);
					hands[i].appUp = (hands[i].appPressed== false && last == true);
					hands[i].appDown = (hands[i].appPressed == true && last == false);
					
				}	
				if(i == Right)
				{
					Vector2 ohJoy = new Vector2(Input.GetAxis("Right Horizontal"), Input.GetAxis("Right Vertical"));
					hands[i].joystick = ohJoy.normalized;
					
					
					bool last = hands[i].thumbPressed;
					hands[i].thumbPressed = !Mathf.Approximately(Input.GetAxis("Right Pad Pressed"), 0f);
					hands[i].thumbUp = (hands[i].thumbPressed == false && last == true);
					hands[i].thumbDown = (hands[i].thumbPressed == true && last == false);
						
					
					
					 last = hands[i].triggerPressed;
					hands[i].triggerPressed = !Mathf.Approximately(Input.GetAxis("Right Trigger"), 0f);
					hands[i].triggerUp = (hands[i].triggerPressed == false && last == true);
					hands[i].triggerDown = (hands[i].triggerPressed == true && last == false);
					
					 last = hands[i].gripPressed;
					hands[i].gripPressed = !Mathf.Approximately(Input.GetAxis("Right Grip"), 0f);
					hands[i].gripUp = (hands[i].gripPressed == false && last == true);
					hands[i].gripDown = (hands[i].gripPressed == true && last == false);
					
					last = hands[i].appPressed;
					hands[i].appPressed = !Mathf.Approximately(Input.GetAxis("Right Menu"), 0f);
					hands[i].appUp = (hands[i].appPressed== false && last == true);
					hands[i].appDown = (hands[i].appPressed == true && last == false);

				}
				
				
			}
			
		}
	
	for(int i = 0; i < 2; i++)
	{
		Transform t = leftHand.transform;
			if(i == Right)
				t = rightHand.transform;
			if(i == Left)
				t = leftHand.transform;
		    Quaternion deltaRotation = t.rotation * Quaternion.Inverse(hands[i].realQuaternion);
     
			
     
			deltaRotation.ToAngleAxis(out var angle, out var axis);
     
			angle *= Mathf.Deg2Rad;
     
			hands[i].angularVelocity = (1.0f / Time.deltaTime) * angle * axis;
			
			hands[i].linearAcceleration = t.position - hands[i].realPosition;

	}

	hands[Left].realPosition = leftHand.transform.position;
	hands[Left].realEulerAngles = leftHand.transform.eulerAngles;
	hands[Left].realQuaternion = leftHand.transform.rotation;
	hands[Right].realPosition = rightHand.transform.position;
	hands[Right].realEulerAngles = rightHand.transform.eulerAngles;
	hands[Right].realQuaternion = rightHand.transform.rotation;
    }
}
