﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoilerRoot : MonoBehaviour
{
	public static int Count = 0;
    // Start is called before the first frame update
    void Awake()
    {
		Count++;
        Object.DontDestroyOnLoad(transform);	
        //Load, no matter what.
        GameDataHolder.LoadFile();  
    }
	
	void OnDestroy()
	{
		Count--;
	}

    

    // Update is called once per frame
    void Update()
    {
        
    }
}
