﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupObject : MonoBehaviour
{
	public MeshRenderer[] meshRend;
	public Rigidbody rigid;
	
	public const float THROW_FORCE = 80f;
	
	public static PickupObject CurrentRight, CurrentLeft;
	public enum WHICH_HAND { None, Left, Right };
	protected WHICH_HAND currentHandHovering;
	
	protected Transform originalParent;
	protected Vector3 originalLocalScale;
	
	public virtual void OnTrigger(bool isRight)
	{
	}
	
	public virtual void OnTriggerUpdate(bool isRight)
	{
	}
	
	
    // Start is called before the first frame update
    void Start()
    {
		originalLocalScale = transform.localScale;
        originalParent = transform.parent;
		OnStart();
    }
	
	protected virtual void OnStart()
	{
	}

	public void OnHover(bool rightHand)
	{
		if(currentHandHovering != WHICH_HAND.None)  //Already hovering
			return;
		if(currentHandHovering == WHICH_HAND.Right && CurrentRight != null)
			return;
		if(currentHandHovering == WHICH_HAND.Left && CurrentLeft != null)
			return;
		for(int i = 0; i < meshRend.Length; i++)
		{
			meshRend[i].material.SetColor("_EmissionColor", new Color(0.2f, 0.2f, 0f, 1f));
		}
		if(rightHand)
			currentHandHovering = WHICH_HAND.Right;
		else
			currentHandHovering = WHICH_HAND.Left;
	}
	
	public void OnEndHover(bool rightHand)
	{
		
		for(int i = 0; i < meshRend.Length; i++)
		{
			meshRend[i].material.SetColor("_EmissionColor", new Color(0f, 0f, 0f, 1f));
		}
		
		currentHandHovering = WHICH_HAND.None;
		
		if(CurrentLeft == this)
		{
			transform.parent = originalParent;
			transform.localScale = originalLocalScale;
			CurrentLeft = null;
		}
		if(CurrentRight == this)
		{
			transform.parent = originalParent;
			transform.localScale = originalLocalScale;
			CurrentRight = null;
		}
	}

	protected virtual void OnUpdate()
	{
	}
    // Update is called once per frame
    void FixedUpdate()
    {
		Vector3 mainForce = Vector3.zero;
		Vector3 angForce = Vector3.zero;
        if(currentHandHovering == WHICH_HAND.Right)
		{
			if(CrossVR.hands[CrossVR.Right].gripPressed)
			{
				CurrentRight = this;
				transform.parent = CrossVR.RightHandTransform;
				//transform.localScale = originalLocalScale;
			}
			else
			{
				if(CurrentRight == this)
				{
					CurrentRight = null;
					transform.parent = originalParent;
					transform.localScale = originalLocalScale;
					mainForce = CrossVR.hands[CrossVR.Right].linearAcceleration * THROW_FORCE;
					angForce = CrossVR.hands[CrossVR.Right].angularVelocity * THROW_FORCE;
				}
			}
		}
		if(currentHandHovering == WHICH_HAND.Left)
		{
			if(CrossVR.hands[CrossVR.Left].gripPressed)
			{
				CurrentLeft = this;
				transform.parent = CrossVR.LeftHandTransform;
				//transform.localScale = originalLocalScale;
			}
			else
			{
				if(CurrentLeft == this)
				{
					CurrentLeft = null;
					transform.parent = originalParent;
					transform.localScale = originalLocalScale;
					mainForce = CrossVR.hands[CrossVR.Left].linearAcceleration * THROW_FORCE;
					angForce = CrossVR.hands[CrossVR.Left].angularVelocity * THROW_FORCE;
				}
			}
		}
		
		if(transform.parent != originalParent)
		{
			rigid.constraints = RigidbodyConstraints.FreezeAll;
		}
		else
		{
			rigid.constraints = RigidbodyConstraints.None;
			if(mainForce != Vector3.zero)
			{
				rigid.AddForce(mainForce, ForceMode.Impulse);
				rigid.AddTorque(angForce, ForceMode.Impulse);
			}
		}
		
		if(CurrentLeft == this)
		{
			if(CrossVR.hands[CrossVR.Left].triggerPressed)
				OnTriggerUpdate(false);
			if(CrossVR.hands[CrossVR.Left].triggerDown)
				OnTrigger(false);
		}
		
		if(CurrentRight == this)
		{
			if(CrossVR.hands[CrossVR.Right].triggerPressed)
				OnTriggerUpdate(true);
			if(CrossVR.hands[CrossVR.Right].triggerDown)
				OnTrigger(true);
		}
		
		OnUpdate();
    }
}
