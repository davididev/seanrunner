﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadRotator : MonoBehaviour
{
	
	public static Vector3 RealPosition, RealEulerAngles;
	public const float ROTATE_SPEED = 90f;
	
	private float startTimer = 1f;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if(startTimer > 0f)
		{
			startTimer -= Time.deltaTime;
			return;
		}
        RealPosition = CrossVR.CurrentHMD.position;
		RealEulerAngles = CrossVR.CurrentHMD.eulerAngles;
		
		Vector2 axis = CrossVR.hands[CrossVR.Right].joystick;
		//Debug.Log("Axis: " + axis);
		if(!Application.isEditor)
			axis = Vector2.zero;  //Don't look up/down with trackpad unless we're in the editor.
		
		
		if(axis != Vector2.zero)
		{
			Vector3 rotVec = transform.eulerAngles;
			rotVec.y = rotVec.y + (ROTATE_SPEED * axis.x * Time.deltaTime);
			if(axis.y < 0f)
				rotVec.x = Mathf.MoveTowardsAngle(rotVec.x, 80f, ROTATE_SPEED * Time.deltaTime);
			if(axis.y > 0f)
				rotVec.x = Mathf.MoveTowardsAngle(rotVec.x, -80f, ROTATE_SPEED * Time.deltaTime);
			transform.eulerAngles = rotVec;
		}
    }
}
