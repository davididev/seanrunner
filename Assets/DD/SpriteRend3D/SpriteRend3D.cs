﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteRend3D : MonoBehaviour
{
	
	[SerializeField] public bool lockX = false;
    [SerializeField] public bool lockY = false;
    [SerializeField] public bool lockZ = false;
	
	[SerializeField] public bool eightDirections = false;
	
	[SerializeField] public Sprite rend0;
	[SerializeField] public Sprite rend45;
	[SerializeField] public Sprite rend90;
	[SerializeField] public Sprite rend135;
	[SerializeField] public Sprite rend180;
	[SerializeField] public Sprite rend225;
	[SerializeField] public Sprite rend270;
	[SerializeField] public Sprite rend315;
	
	[SerializeField] public Color emmissive = Color.black;
	[SerializeField] public Color diffuse = Color.white;
	
	private SpriteRenderer rend;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if(rend == null)
			rend = GetComponent<SpriteRenderer>();
		
		//Rotate
		Vector3 rel = transform.position - CrossVR.CurrentHMD.position;
            Quaternion q1 = Quaternion.identity;
			if (rel != Vector3.zero)
				q1 = Quaternion.LookRotation(rel);
            
            Vector3 currentAngles = transform.eulerAngles;
            Vector3 newAngles = q1.eulerAngles;

            //If any axises are locked, don't rotate (go back to the angles BEFORE you rotated)
            if (lockX)
                newAngles.x = currentAngles.x;
            if (lockY)
                newAngles.y = currentAngles.y;
            if (lockZ)
                newAngles.z = currentAngles.z;


            transform.eulerAngles = newAngles;
		
		//Set the sprites
		if(eightDirections)
		{
			Vector3 dir = CrossVR.CurrentHMD.InverseTransformDirection(transform.parent.forward);
			float angle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
			
			
			if(Mathf.DeltaAngle(angle, 0f + 22.5f) < 23.0f)
			{
				rend.sprite = rend0;
				return;
			}
			if(Mathf.DeltaAngle(angle, 45f + 22.5f) < 23.0f)
			{
				rend.sprite = rend45;
				return;
			}
			if(Mathf.DeltaAngle(angle, 90f + 22.5f) < 23.0f)
			{
				rend.sprite = rend90;
				return;
			}
			if(Mathf.DeltaAngle(angle, 135f + 22.5f) < 23.0f)
			{
				rend.sprite = rend135;
				return;
			}
			if(Mathf.DeltaAngle(angle, 180f + 22.5f) < 23.0f)
			{
				rend.sprite = rend180;
				return;
			}
			if(Mathf.DeltaAngle(angle, 225f + 22.5f) < 23.0f)
			{
				rend.sprite = rend225;
				return;
			}
			if(Mathf.DeltaAngle(angle, 270f + 22.5f) < 23.0f)
			{
				rend.sprite = rend270;
				return;
			}
			if(Mathf.DeltaAngle(angle, 315f + 22.5f) < 23.0f)
			{
				rend.sprite = rend315;
				return;
			}
			
		}
		else  //Not eight directions (still set sprites)
		{
			rend.sprite = rend0;
		}
		
		rend.material.SetColor("_EmissionColor", emmissive);
		rend.material.SetColor("_Albedo", diffuse);
    }
}
