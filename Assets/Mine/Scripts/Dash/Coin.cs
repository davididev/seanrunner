﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
	public AudioClip coinSound;
	public const float DEFAULT_PITCH= 1f;
	
	public static float pitch = DEFAULT_PITCH;
	public static float lastTime = 0f;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider c)
    {
		if(c.tag != "Player")
			return;
		float diff = Time.time - lastTime;
		if(diff < 0.5f)
			pitch += 0.2f;
		else
			pitch = DEFAULT_PITCH;
		
        lastTime = Time.time;
		
		GameObject snd = GameObjectPool.GetInstance("Coin Sound", CrossVR.CurrentHMD.position, Quaternion.identity);
		AudioSource src = snd.GetComponent<AudioSource>();
		src.pitch = pitch;
		src.Play();
		
		
		SceneVars.Score++;
		
		
		GameDataHolder.instance.gold++;
		if(GameDataHolder.instance.cheatDoubleCoins)
			GameDataHolder.instance.gold++;  //Get two instead of one!
		
		gameObject.SetActive(false);
    }
}
