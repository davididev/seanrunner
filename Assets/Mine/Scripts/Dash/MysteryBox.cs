﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MysteryBox : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void AddItem()
	{
		int chance = Random.Range(0, 100);
		//chance = 5;  //Testies!
		if(chance >= 0 && chance <= 65) //coins
		{
			int amt = Random.Range(50, 200);
			GameDataHolder.instance.gold += amt;
			PlayerUI.PowerupText = amt + " coins";
			
			SceneVars.Score += amt;
		}
		if(chance >= 67 && chance <= 90) //fireballs
		{
			int amt = Random.Range(2, 10);
			GameDataHolder.instance.fireBalls += amt;
			PlayerUI.PowerupText = amt + " fireballs";
		}
		if(chance >= 91 && chance <= 99) //ankh
		{
			int amt = Random.Range(1, 3);
			GameDataHolder.instance.ankhs += amt;
			PlayerUI.PowerupText = amt + " ankhs";
		}
		if(chance == 100) //coin jackpot
		{
			int amt = 1000000;
			GameDataHolder.instance.gold += amt;
			PlayerUI.PowerupText = "COIN JACKPOT!!!\n1 MIL COINS!!!";
			
			SceneVars.Score += amt;
		}
	}
	
	public void OnSmash()  //Message called by weapon or fireball
	{
			GameObjectPool.GetInstance("Cube Explosion", transform.position, Vector3.zero);
			//Add items later
			AddItem();
			gameObject.SetActive(false);
	}
}
