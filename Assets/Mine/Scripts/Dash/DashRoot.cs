﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashRoot : MonoBehaviour
{
	public Transform pickupsList;
    // Start is called before the first frame update
    void OnEnable()
    {
        foreach(Transform t in pickupsList.transform)
		{
			t.gameObject.SetActive(true);
		}
    }

    // Update is called once per frame
    void Update()
    {
        float currentZ = HeadRotator.RealPosition.z;
		float dist = transform.position.z - currentZ;
		
		if(dist <= -200f)
			gameObject.SetActive(false);		
		
    }
}
