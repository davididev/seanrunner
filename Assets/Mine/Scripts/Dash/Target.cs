﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void OnSmash()  //Message called by weapon or fireball
	{
		GameObjectPool.GetInstance("Target Explosion", transform.position, Vector3.zero);
			SceneVars.Score += 100;
			gameObject.SetActive(false);
	}
}
