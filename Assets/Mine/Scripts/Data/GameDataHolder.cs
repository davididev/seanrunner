﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GameDataHolder : MonoBehaviour
{
    public static GameData instance = new GameData();
    //public static int fileID = 0;


    public static void LoadFile()
    {
		instance = null;
		
        string fileName = Application.persistentDataPath + "/file.dat";
		if(!File.Exists(fileName))
			instance = new GameData();
		
        StreamReader read = new StreamReader(fileName);
        instance = (GameData) JsonUtility.FromJson(read.ReadToEnd(), typeof(GameData));
        read.Close();
    }

    public static void SaveFile()
    {
        string f = JsonUtility.ToJson(instance);
        string fileName = Application.persistentDataPath + "/file.dat";
        StreamWriter write = new StreamWriter(fileName);
        write.Write(f);
        write.Close();

    }
}
