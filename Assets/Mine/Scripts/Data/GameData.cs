﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData 
{
	
	[SerializeField] public int highScore = 0;
	[SerializeField] public int fireBalls = 3;
	[SerializeField] public int gold = 0;
	[SerializeField] public int ankhs = 0;
	
	[SerializeField] public bool cheatDoubleCoins = false, cheatSlowSpeed = false;
	
}
