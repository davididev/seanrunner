﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	public const float MIN_SPEED = 3f, MAX_SPEED = 20f, ACCELERATION_PER_HUNDRED = 1f, JUMP_HEIGHT = 3f, SUPER_JUMP_HEIGHT = 7f, STRAFE_SPEED = 15f;
	
	public const float CROUCH_HEIGHT = 0.4f, REGULAR_HEIGHT = 1.9f, START_TIME = 2f;
	
	public static float currentSpeed = MIN_SPEED, gravity = 0f, jumpDestination = -100f;
	
	public CharacterController ccm;
	
	private int xDest = 0;
	private bool lastMove = true;
	public static bool DontMove = false;
	
	public Transform headRotator;
	private float startTimer = 0f;
	public GameObject mysteryBoxExplosionPrefab, targetExplosionPrefab, coinSoundPrefab, fireballPrefab, fireballExplosionPrefab;
	
    // Start is called before the first frame update
    void Start()
    {
		currentSpeed = MIN_SPEED;
        ccm.height = REGULAR_HEIGHT;
		GameObjectPool.InitPoolItem("Cube Explosion", mysteryBoxExplosionPrefab, 10);
		GameObjectPool.InitPoolItem("Target Explosion", targetExplosionPrefab, 10);
		GameObjectPool.InitPoolItem("Coin Sound", coinSoundPrefab, 10);
		GameObjectPool.InitPoolItem("Fireball", fireballPrefab, 3);
		GameObjectPool.InitPoolItem("Fireball Explosion", fireballExplosionPrefab, 3);
    }

    // Update is called once per frame
    void Update()
    {
		if(startTimer > 0f)
		{
			startTimer -= Time.deltaTime;
			return;
		}
        if(ccm.isGrounded)
			gravity = 0f;
		else	
		{
			if(jumpDestination > -100f)
			{
				float dist = jumpDestination - transform.position.y;
				gravity += 9.8f * Time.deltaTime;
				if(gravity > dist)
				{
					gravity = dist;
					jumpDestination = -100f;
				}
			}
			else
				gravity -= 9.8f * Time.deltaTime;
		}
		
		Vector2 mv = CrossVR.hands[CrossVR.Left].joystick;
		if(!Application.isEditor)
			mv += CrossVR.hands[CrossVR.Right].joystick;
		if(mv == Vector2.zero)
			lastMove = true;
		
		if(lastMove && mv.x < -0.5f)
		{
			lastMove = false;
			xDest--;
			if(xDest < -1)
				xDest = -1;
		}
		if(lastMove && mv.x > 0.5f)
		{
			lastMove = false;
			xDest++;
			if(xDest > 1)
				xDest = 1;
		}
		
		if(lastMove && mv.y > 0.5f)
		{
			if(ccm.isGrounded)
			{
				jumpDestination = transform.position.y + JUMP_HEIGHT;
				lastMove = false;
			}
		}
		
		if(lastMove && mv.y < -0.5f)
		{
			if(ccm.isGrounded && crouching == false)
			{
				StartCoroutine(CrouchRoutine());
				lastMove = false;
			}
		}
		
		if(ccm.isGrounded)
			currentSpeed += ACCELERATION_PER_HUNDRED / 100f * Time.deltaTime;
		
		float x = 0f;
		if(xDest == -1)
			x = -2f;
		if(xDest == 1)
			x = 2f;
		
		if(DontMove == false)
		{
			float xDestination = Mathf.MoveTowards(transform.position.x, x, STRAFE_SPEED * Time.deltaTime);
			Vector3 moveVec = new Vector3(xDestination - transform.position.x, gravity, currentSpeed * Time.deltaTime);
		
			ccm.Move(moveVec);
			
			
		}
		
		headRotator.transform.position = transform.position + (Vector3.up * ccm.height * 0.4f);
    }
	
	private bool crouching = false;
	public IEnumerator CrouchRoutine()
	{
		crouching = true;
		ccm.height = CROUCH_HEIGHT;
		yield return new WaitForSeconds(1f);
		ccm.height = REGULAR_HEIGHT;
		
		crouching = false;
	}
}
