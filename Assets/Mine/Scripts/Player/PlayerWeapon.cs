﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{
	public const float AUDIO_TIMER = 0.3f;
	private float audioTimer = 0f;
	
	public AudioClip swingFx;
	
	public BoxCollider boxCol;
	
	public enum HAND_ID { RIGHT = 0, LEFT = 1 };
	public HAND_ID handID = HAND_ID.LEFT;
	
	public TrailRenderer trailRend;
	public ParticleSystem ps;
	
	public Transform fireballPoint;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        int id = (int) handID;
		bool isSwinging = false;
		if(CrossVR.hands[id].angularVelocity.sqrMagnitude > 0.5f)
			isSwinging = true;
		if(CrossVR.hands[id].linearAcceleration.sqrMagnitude > 0.25f)
			isSwinging = true;
		
		
		trailRend.emitting = isSwinging;
		if(isSwinging)
		{
			if(audioTimer <= 0f)
			{
				AudioSource.PlayClipAtPoint(swingFx, transform.position);
				audioTimer = AUDIO_TIMER;
			}
			
			Collider[] c = PhysicsExtensions.OverlapBox(boxCol, LayerMask.GetMask("Pickup", "Box"));
			if(c != null)
			{
				for(int i = 0; i < c.Length; i++)
				{
					c[i].gameObject.SendMessage("OnSmash", SendMessageOptions.DontRequireReceiver);
				}
			}
		}
		
		if(audioTimer > 0f)
			audioTimer -= Time.deltaTime;
		
		
		if(GameDataHolder.instance.fireBalls > 0)
		{
			if(CrossVR.hands[CrossVR.Left].triggerDown && handID == HAND_ID.LEFT)
			{
				GameObject fb = GameObjectPool.GetInstance("Fireball", fireballPoint.position, fireballPoint.rotation);
				if(fb != null)
				{
					fb.SendMessage("SetVelocity", PlayerMovement.currentSpeed);
					GameDataHolder.instance.fireBalls--;
				}
			}
			if(CrossVR.hands[CrossVR.Right].triggerDown && handID == HAND_ID.RIGHT)
			{
				GameObject fb = GameObjectPool.GetInstance("Fireball", fireballPoint.position, fireballPoint.rotation);
				if(fb != null)
				{
					fb.SendMessage("SetVelocity", PlayerMovement.currentSpeed);
					GameDataHolder.instance.fireBalls--;
				}
			}
		}

    }
}
