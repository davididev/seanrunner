﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneVars : MonoBehaviour
{
	public GameObject[] geometries;
	public GameObject Boilerplate;
	private int max = 0;
	
	public static int Score = 0;
    // Start is called before the first frame update
    void Start()
    {
		if(BoilerRoot.Count == 0)
		{
			GameObject bp = GameObject.Instantiate(Boilerplate, Vector3.zero, Quaternion.identity) as GameObject;
		}
		if(geometries == null)
		{
			PlayerMovement.DontMove = true;
			return;
		}
		if(geometries.Length == 0)
		{
			PlayerMovement.DontMove = true;
			return;
		}
		Score = 0;  //Reset score, but only in game.
		PlayerMovement.DontMove = true;
		
		max = -1;
        for(int i = 0; i < geometries.Length; i++)
		{
			max++;
			GameObjectPool.InitPoolItem("Geo" + i, geometries[i], 4);
		}
		
		Invoke("Startup", 0.2f);
		
    }
	
	void Startup()
	{
		SpawnAGeo(0);
		SpawnAGeo(1);
		SpawnAGeo(-1);
		PlayerMovement.DontMove = false;
	}
	
	void SpawnAGeo(int unitZ)
	{
		int rand = Random.Range(0, max);
		float z = (float) unitZ;
		GameObject g = GameObjectPool.GetInstance("Geo" + rand, new Vector3(0f, -2f, z * 100f), Quaternion.identity);
		g.SetActive(true);
	}

    // Update is called once per frame
	float previousZ = 0f;
    void Update()
    {
        if(PlayerMovement.DontMove == false)
		{
			int oldUnitZ = Mathf.FloorToInt(previousZ / 100f);
			int newUnitZ = Mathf.FloorToInt(CrossVR.CurrentHMD.position.z / 100f);
			previousZ = CrossVR.CurrentHMD.position.z;
			
			if(oldUnitZ != newUnitZ)
			{
				SpawnAGeo(newUnitZ + 1);
				SceneVars.Score += 100;
			}
		}
    }
}
