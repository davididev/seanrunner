﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
	public const float VELOCITY = 20f, LIFETIME = 5f;
	private float myVelocity;
	private float life = 0f;
	
	private Rigidbody rigid;
    // Start is called before the first frame update
    void OnEnable()
    {
        life = 0f;
    }
	
	public void SetVelocity(float v)
	{
		myVelocity = v + VELOCITY;
	}

    // Update is called once per frame
    void FixedUpdate()
    {
        if(rigid == null)
			rigid = GetComponent<Rigidbody>();
		
		rigid.velocity = transform.forward * myVelocity;
		life += Time.fixedDeltaTime;
		if(life > LIFETIME)
			gameObject.SetActive(false);
		
		Collider[] c = Physics.OverlapSphere(transform.position, 1f, LayerMask.GetMask("Pickup", "Box"));
			if(c != null)
			{
				for(int i = 0; i < c.Length; i++)
				{
					c[i].gameObject.SendMessage("OnSmash", SendMessageOptions.DontRequireReceiver);
				}
				if(c.Length > 0)
					Splode();
			}
    }
	
	void OnCollisionEnter(Collision c)
	{
		Splode();
	}
	
	void Splode()
	{
		GameObject splosion = GameObjectPool.GetInstance("Fireball Explosion", transform.position, Quaternion.identity);
		splosion.transform.forward = transform.forward;
		gameObject.SetActive(false);
	}
}
