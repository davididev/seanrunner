﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
	public TMPro.TextMeshProUGUI scoreText, coinText, powerupDisplay, fireballText;
	
	public static string PowerupText = "";
	private bool connected = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if(connected == false)
		{
			if(CrossVR.CurrentHMD != null)
			{
				transform.parent = CrossVR.CurrentHMD;
				transform.localPosition = new Vector3(0f, 0f, 2f);
				connected = true;
			}
		}
        scoreText.text = System.String.Format("{0:n0}", SceneVars.Score);
		coinText.text = System.String.Format("{0:n0}", GameDataHolder.instance.gold);
		fireballText.text = System.String.Format("{0:n0}", GameDataHolder.instance.fireBalls);
		
		if(PowerupText != "")
		{
			StartCoroutine(PowerupTextRoutine());
		}
    }
	
	IEnumerator PowerupTextRoutine()
	{
		string s = PowerupText;
		PowerupText = "";
		
		powerupDisplay.gameObject.SetActive(true);
		powerupDisplay.text = s;
		
		yield return new WaitForSeconds(2f);
		powerupDisplay.gameObject.SetActive(false);
	}
}
